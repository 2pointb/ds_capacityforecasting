args = commandArgs(trailingOnly = TRUE)



if (length(args) == 0) {
  stop("path must be supplied (input file).n", call. = FALSE)
} 

print(args[1])

wd <- args[1]

source(paste(wd, "DriverOrigin2.R", sep = ''))
#source(paste(wd, "connectRdb.R", sep = ''))
source(paste(wd, "connectR.R", sep = ''))

region_id <- list()
region_id[[1]] <- 'Phoenix'
region_id[[2]] <- 'Tucson'
region_id[[3]] <- 'Boise'

# 
# friendly_string_for_count_qry <- function(drivers_regions, region_id){
#   drivers_regions<-subset(drivers_regions, RegionId >0)
#   l <- c()
#   for(i in 1:nrow(drivers_regions)){
#   #for(i in 1:3){
#     l <- c(l, paste("(",paste(drivers_regions$DriverId[i], paste("'", region_id[[drivers_regions$Region[i]]], "'", sep = ''), sep = ','),')', sep =''))
#   }
#   
#   out <- paste(l, collapse = ",")
#   return(out)
# }



friendly_string_for_count_qry <- function(drivers_regions, region_id){
  drivers_regions<-subset(drivers_regions, RegionId >0)
  
  l <- c()
  for(i in 1:nrow(drivers_regions)){
    #for(i in 1:3){
    uid <- paste("'", drivers_regions$Uid[i], "'", sep = '')
    metro <- paste("'", region_id[[drivers_regions$RegionId[i]]], "'", sep = '')
    l <- c(l, paste('(',uid,',',metro,')', sep = ''))
  }
  
  out <- paste(l, collapse = ",")
  return(out)
}




#update_driver_regions(bbox_list)


drivers_regions <- get_from_db('Select * from DriversRegions', 'DataSci', db_list)
drivers_regions$RegionId <- as.numeric(drivers_regions$RegionId)


# out <- friendly_string_for_count_qry(head(drivers_regions,990), region_id)




break_up_query <- function(drivers_regions, insert_init, insert2){
  denominator <- 450
  n <- nrow(drivers_regions)
  n_segs <- round(n/denominator)
  s_seq <- round(seq(1, n, length.out = n_segs+1))
  l <- list()
  for (i in 1:(length(s_seq) - 1)){
    first_pos <- s_seq[i]
    last_pos <- s_seq[i+1] - 1
  
    driver_slice <- drivers_regions[first_pos:last_pos,]
    if(all(driver_slice$RegionId==0)){next()}
    out <- friendly_string_for_count_qry(driver_slice, region_id)
    #print(out)
    l[[i]] <- out
  }
  l <- l[!sapply(l, is.null)]
  q <- c()
  for(i in 1:length(l)){
    if(i ==1){
      seg <- paste(insert_init, l[[i]])
    }
    
    else{
     seg <- paste(insert2, l[[i]])
    }
    q <- paste(q, seg, '\n\n')
  }
  
  return(q)
}


insert_init <- "\n DECLARE @drivers TABLE (DriverID UNIQUEIDENTIFIER, Location varchar(50))
INSERT INTO @drivers( DriverID, Location )
VALUES"

insert2 <- "\n INSERT INTO @drivers( DriverID, Location )
VALUES"



out <- break_up_query(drivers_regions, insert_init, insert2)










blah1 <-
  "SET TRAN ISOLATION LEVEL READ UNCOMMITTED

DECLARE @startTime DATETIME = CONVERT(DATETIME,CONVERT(DATE,DATEADD(DAY,-200,GETDATE())))
DECLARE @startCounter DATETIME = @startTime
DECLARE @endTime DATETIME = DATEADD(HOUR,DATEPART(HOUR,GETDATE()),CONVERT(DATETIME,CONVERT(DATE,GETDATE())))
DECLARE @time TABLE (Time DATETIME)
--SELECT @startTime,@endTime,GETDATE()"


blah2 <-   "SELECT
h.PartitionKey AS Driver
, h.Date AS Time
, REVERSE(SUBSTRING(
REVERSE(h.Details)
, CHARINDEX('\"',REVERSE(h.Details))+1
, CHARINDEX('\"',REVERSE(h.Details),CHARINDEX('\"',REVERSE(h.Details))+1)-CHARINDEX('\"',REVERSE(h.Details))-1
)) AS DriverStatus
, d.Location
INTO #results
--SET TRAN ISOLATION LEVEL READ UNCOMMITTED select top 100 *
FROM dbo.HistoryLogs h
LEFT JOIN @drivers d ON d.DriverID=h.PartitionKey
WHERE 1=1
AND h.EventType=1002    
AND h.OperatorId in (3,67)
AND h.Date >= @startTime
AND h.Date < @endTime

WHILE(@startCounter < @endTime)
BEGIN
INSERT INTO @time
VALUES (@startCounter)
SET @startCounter = DATEADD(HOUR,1,@startCounter)
END    

; WITH foo AS (
SELECT DISTINCT
x.Driver
, y.Time AS Time
, x.Location
, NULL AS DriverStatus
FROM #results x
CROSS APPLY @time y
UNION ALL
SELECT 
z.Driver
, z.Time
, z.Location
, z.DriverStatus
FROM #results z
)

, foo2 AS (
SELECT 
CONVERT(DATE,x.Time) AS Date
, DATEPART(HOUR,x.Time) AS Hour
, x.Driver
, x.DriverStatus
, x.Location
, COUNT(x.DriverStatus) OVER (PARTITION BY x.Driver ORDER BY x.Time) AS c
FROM foo x
)

, foo3 AS (
SELECT
x.Date
, x.Hour
, MAX(x.DriverStatus) OVER (PARTITION BY x.Driver, x.c) AS DriverStatus
, x.Driver
, x.Location
FROM foo2 x
)
SELECT
x.Location
, DATEADD(HOUR,x.Hour,CONVERT(DATETIME,x.Date)) AS DateTime
, COUNT(DISTINCT x.Driver) AS DistinctDriverCount
FROM foo3 x
WHERE 1=1
AND x.DriverStatus IN ('Available','Busy')
AND x.Date > @startTime
GROUP BY
x.Date
, x.Hour
, x.Location
ORDER BY
Location
, DateTime
DROP TABLE #results"




qry <- paste(blah1,out,blah2)


write(qry, file = paste(wd, 'qry.txt', sep = ''))

