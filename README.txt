
Capacity Calculation Project


$project
========

#The purpose of the this package is to easily use a set of tools to control the distribution schedule for RideShare regions at Veyo.
	
The primary output of this package is a forecast of the number of trips to keep for IDPs for the short term future published to a server for use in trip distribution.

The program can operate in autonomous mode where it is constantly learning and generating forecasts. Launching in new markets where there is no history requires the use of manual settings found in the settings, or -config, file. This is a place where one can heuristacally enter values based on all prior knowledge going into a new market.
The config file is available to set variuos parameters: control for operating hours, minimum dispatch requirements, forecast duration, among other things.



$forecast method
================


Capacity Forecasting

This function of the algorithm forecsts count values for a sequence of consecutive hours using Arima model. This is used for both the supply and demand forecasting. 
The specifics of the model is an auto regressive Moving avewage of first order with a weekly cyclical component. Through testing it was found that the first 6 days 
of the model was optimal for the arima model after witch subsitiuting historical houlry averagea are used for the rest of the forecasts duration.

Capacity Forecasting.

First the arima function is used to forecast hourly driver volumes looking at driver active sessions for a specific rideshare region. 
From here the value gets coinverted to a trip count for dispatch by correlating driver count with offered trip count. This is done through 
a polynomial regression model. The results is a value that will have cancelation rates baked into the forecast based on the assumption that 
trips for distribution are selected at random.





#(read config file documentation for more details)


$example
========

Very easy to use:
	#ADD EXEC
	
	//load dependencies//
	//load config file//

	> capacity_calc(config) 



$config file details
===================

	The below config setting dictate how the capacity will operate and where.
	indicate between manual TRUE or FALSE
	set operating schedule
	set test times
	enter manual settings if manual is set to TRUE
	Config file is in YML format which is sensitive to white space so be careful!
	
	config example:
	
	###################################################################################################

        AREA INPUTS

		AreaId: 2043
		region_id: 3
		product_id: 6
		OperatorId: 67
		MarketId: 2
		forecast_length: 16
		time_zone: 'America/Boise'

		
		
		demand_forecasting:
			
			manual: TRUE
		
			
		capacity_forecasting:
		
			manual: TRUE

			manual_settings:
				current_population: 40			#Driver pop
				final_population: 100			#Projected driver pop at end of forecast period
				participation_rate: 1			#Driver Participation Rate
				trip_per_hour: 1.2				#Trips per hour
				max_hourly_capacity: 250		#Max Setting if constant capacity = T
				constant_capacity: TRUE		

		 
				operating_schedule:
				    Monday:
				        start: 6
				        end: 20
				    Tuesday:
				        start: 6
				        end: 20
				    Wednesday:
				        start: 6
				        end: 20
				    Thursday:
				        start: 6
				        end: 20
				    Friday:
				        start: 6
				        end: 20
				    Saturday:
				        start: 6
				        end: 17


				manual_hourly_capacity_override:  #manualy set times of day



		test_mode: FALSE


	###################################################################################################





$dependencies
=============

R packages:
'yaml'
'lubridate'
'scales'
'sp'
'RJDBC'

Python packages:
'pymssql'
'pandas'

pseudo_forecast.R
connectRdb.R
arima.R
Pythonqry.py
py_poly.py



$flowchart for capacity_calc()
==============================



                                      +-------------+
                                      | READ CONFIG |
                                      +------+------+
                                             |
            'supply/capacity'        +-------v--------+            'demand'
                                     |                |
                    +----------------+ SET START TIME +----------------+
                    |                |                |                |
                    |                +----------------+                |
        +-----------v------------+                                     |
        |                        |                                     |
      +-+ create_capacity_calc() +-----+                               |
      | |                        |     |                 +-------------v------------+
      | +------------------------+     |                 |                          |
      |                                |                 | create_demand_forecast() |
+-----v---------+         +------------v---------+       |                          |
|               |         |                      |       +-------------+------------+
| auto_supply() |         | find_manual_supply() |                     |
|               |         |                      |                     |
+-----+---------+         +------------+---------+       +-------------v--------------+
      |                                |                 |                            |
      |  +-----------------------+     |                 | find_artificial_forecast() |
      |  |                       |     |                 |                            |
      +--> convert_to_capacity() <-----+                 +-------------+--------------+
         |                       |                                     |
         +----------+------------+                                     |
                    |                                                  |
                    |        +-----------------------------+           |
                    |        |                             |           |
                    +--------> create_distribution_table() <-----------+
                             |                             |
                             +-------------+---------------+
                                           |
                                 +---------v------------+
                                 |                      |
                                 | manual_adjustments() |  #### OPERATING 
                                 |                      |
                                 +---------+------------+
                                           |
                                 +---------v-----------+
                                 |                     |
                                 |  adjust_for_test()  |
                                 |                     |
                                 +---------+-----------+
                                           |
                                 +---------v----------------+
                                 |                          |
                                 | adjust_total_trips_max() |
                                 |                          |
                                 +---------+----------------+
                                           |
                                +----------v-----------+
                                |                      |
                                |  plot_distribution() |
                                |                      |
                                +----------+-----------+
                                           |
                                 +---------v---------+
                                 |                   |
                                 | FORMAT FOR OUTPUT |
                                 |                   |
                                 +---------+---------+
                                           |
                             +-------------v----------------+
                             |                              |
                             |  to.csv(distribution_table)  |
                             |                              |
                             +------------------------------+




$Support
========

If you are having issues, please email data@veyo.com.





